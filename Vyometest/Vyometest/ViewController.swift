//
//  ViewController.swift
//  Vyometest
//
//  Created by Click Labs on 3/25/15.
//  Copyright (c) 2015 clabs. All rights reserved.
//

import UIKit

var toDoItems:[String] = ["Hello world!!"]

class ViewController: UIViewController, UITableViewDelegate, UITextFieldDelegate  {

    
    @IBOutlet weak var profileImage: UIImageView!
    
    var gen = 0
    
    @IBOutlet weak var displayGenderLabel: UILabel!
    @IBOutlet weak var selectGenderButton: UIButton!
    
    @IBAction func selectGenderButton(sender: UIButton) {
        
        if (gen == 0) {
            
            displayGenderLabel.text = "Male"
            gen++
        }else if(gen == 1) {
            displayGenderLabel.text = "Female"
            gen = 0
        }
        

    }
    
  /*@IBOutlet weak var moreUIView: UIView!
    @IBOutlet weak var calendarView: UIView!
    @IBOutlet weak var customerView: UIView!
    @IBOutlet weak var todayView: UIView!
    @IBOutlet weak var homeView: UIView!
    
    
   // @IBOutlet weak var bookHistoryTable: UITableView!
 
    @IBOutlet weak var genderLabel: UILabel!
   
    @IBOutlet weak var profileImage: UIImageView!
    
    
    
    
    func UIViewColor(){
    
        moreUIView.backgroundColor =  UIColor(red: 174/255, green: 13/255, blue: 59/255, alpha: 1)
    }
    */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
      
      self.profileImage.layer.cornerRadius = self.profileImage.frame.size.width / 2;
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    

}

