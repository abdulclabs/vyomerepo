//
//  EditAppointmentViewController.swift
//  Vyometest
//
//  Created by Click Labs on 4/1/15.
//  Copyright (c) 2015 clabs. All rights reserved.
//

import UIKit

class EditAppointmentViewController: UIViewController, UITextViewDelegate  {

 
   // let y:Int = 2
    var y = CGFloat(40)
    
    var duration = 0
    var discount = 0
    
    @IBOutlet weak var serviceUIView: UIView!
    
    @IBOutlet weak var addMoreServiceButton: UIButton!
    
    @IBAction func addMoreServiceButton(sender: UIButton) {
        var label: UILabel = UILabel()
        label.frame = CGRectMake(20, y, 300, 25)
        label.backgroundColor = UIColor.greenColor()
        label.textColor = UIColor.whiteColor()
        label.textAlignment = NSTextAlignment.Left
        label.text = "test label"
        self.serviceUIView.addSubview(label)
        y = y + 26
        
    }
    
    
    
    
    
    
    @IBOutlet weak var editAppointmentLabel: UILabel!
    @IBOutlet weak var timeDateBackgroundLabel: UIView!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var profileNameLabel: UILabel!
    @IBOutlet weak var changeProfileNameLabel: UILabel!
    @IBOutlet weak var servicesLabel: UILabel!
    @IBOutlet weak var addMoreServicesLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var recurringAppointmentLabel: UILabel!
    @IBOutlet weak var writeNotesLabel: UILabel!
    @IBOutlet weak var notesTextView: UITextView!
    @IBOutlet weak var notesLengthLabel: UILabel!
    @IBOutlet weak var costLabel: UILabel!
    @IBOutlet weak var displayCostLabel: UILabel!
    @IBOutlet weak var productCostLabel: UILabel!
    @IBOutlet weak var displayProductCostLabel: UILabel!
    @IBOutlet weak var discountLabel: UILabel!
    @IBOutlet weak var taxLabel: UILabel!
    @IBOutlet weak var displayTaxLabel: UILabel!
    @IBOutlet weak var totalAmountLabel: UILabel!
    @IBOutlet weak var homeLabel: UILabel!
    @IBOutlet weak var todayLabel: UILabel!
    @IBOutlet weak var calenderLabel: UILabel!
    @IBOutlet weak var customerLabel: UILabel!
    @IBOutlet weak var moreLabel: UILabel!
    @IBOutlet weak var displayDurationLabel: UILabel!
     @IBOutlet weak var displayDiscountLabel: UILabel!
    
    @IBOutlet weak var saveAppointmentButton: UIButton!
     @IBOutlet weak var minusDiscountButton: UIButton!
    
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        self.view.endEditing(true)
        
    }
    //code for increse and decrese duration...
    @IBAction func plusDurationButton(sender: UIButton) {
        ++duration
        displayDurationLabel.text = "\(duration)"
    }
    
    @IBAction func minusDuration(sender: UIButton) {
        if duration != 0 {
            --duration
            displayDurationLabel.text = "\(duration)"
        }else if duration == 0 {
            duration = 0
            displayDurationLabel.text = "\(duration)"
        }
    }
    
    //code for increse and decrese discount...
    @IBAction func plusDiscountButton(sender: AnyObject) {
        ++discount
        displayDiscountLabel.text = "\(discount)"
    }
   
    @IBAction func minusDiscountButton(sender: UIButton) {
        if discount != 0 {
            --discount
            displayDiscountLabel.text = "\(discount)"
        }else if discount == 0 {
            discount = 0
            displayDiscountLabel.text = "\(discount)"
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        //set font family and font color...
        editAppointmentLabel.font = (UIFont(name: "Raleway-Medium", size: 17))
        editAppointmentLabel.textColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)
       
        timeDateBackgroundLabel.backgroundColor = UIColor(red: 244/255, green: 244/255, blue: 244/255, alpha: 1)
        
        timeLabel.font = (UIFont(name: "Raleway-Medium", size: 17))
        timeLabel.textColor = UIColor(red: 75/255, green: 75/255, blue: 75/255, alpha: 1)
       
        dateLabel.font = (UIFont(name: "Raleway-Medium", size: 17))
        dateLabel.textColor = UIColor(red: 75/255, green: 75/255, blue: 75/255, alpha: 1)
        
        profileNameLabel.font = (UIFont(name: "Raleway-Medium", size: 17))
        profileNameLabel.textColor = UIColor(red: 75/255, green: 75/255, blue: 75/255, alpha: 1)
       
        changeProfileNameLabel.font = (UIFont(name: "Raleway-Medium", size: 17))
        changeProfileNameLabel.textColor = UIColor(red: 224/255, green: 20/255, blue: 79/255, alpha: 1)
        
        servicesLabel.font = (UIFont(name: "Raleway-Medium", size: 17))
        servicesLabel.textColor = UIColor(red: 75/255, green: 75/255, blue: 75/255, alpha: 1)
        
        addMoreServicesLabel.font = (UIFont(name: "Raleway-Medium", size: 17))
        addMoreServicesLabel.textColor = UIColor(red: 224/255, green: 20/255, blue: 79/255, alpha: 1)
        
        durationLabel.font = (UIFont(name: "Raleway-Medium", size: 17))
        durationLabel.textColor = UIColor(red: 75/255, green: 75/255, blue: 75/255, alpha: 1)
        
        displayDurationLabel.font = (UIFont(name: "Raleway-Medium", size: 20))
        displayDurationLabel.textColor = UIColor(red: 75/255, green: 75/255, blue: 75/255, alpha: 1)

        recurringAppointmentLabel.font = (UIFont(name: "Raleway-Medium", size: 17))
        recurringAppointmentLabel.textColor = UIColor(red: 75/255, green: 75/255, blue: 75/255, alpha: 1)
        
        writeNotesLabel.font = (UIFont(name: "Raleway-Medium", size: 17))
        writeNotesLabel.textColor = UIColor(red: 75/255, green: 75/255, blue: 75/255, alpha: 1)
        
        notesLengthLabel.font = (UIFont(name: "Raleway-Medium", size: 17))
        notesLengthLabel.textColor = UIColor(red: 75/255, green: 75/255, blue: 75/255, alpha: 1)
        
        costLabel.font = (UIFont(name: "Raleway-Medium", size: 17))
        costLabel.textColor = UIColor(red: 75/255, green: 75/255, blue: 75/255, alpha: 1)
        
        displayCostLabel.font = (UIFont(name: "Raleway-Medium", size: 20))
        displayCostLabel.textColor = UIColor(red: 224/255, green: 20/255, blue: 79/255, alpha: 1)
        
        productCostLabel.font = (UIFont(name: "Raleway-Medium", size: 17))
        productCostLabel.textColor = UIColor(red: 75/255, green: 75/255, blue: 75/255, alpha: 1)
        
        displayProductCostLabel.font = (UIFont(name: "Raleway-Medium", size: 20))
        displayProductCostLabel.textColor = UIColor(red: 224/255, green: 20/255, blue: 79/255, alpha: 1)
        
        discountLabel.font = (UIFont(name: "Raleway-Medium", size: 17))
        discountLabel.textColor = UIColor(red: 75/255, green: 75/255, blue: 75/255, alpha: 1)
        
        displayDiscountLabel.font = (UIFont(name: "Raleway-Medium", size: 17))
        displayDiscountLabel.textColor = UIColor(red: 75/255, green: 75/255, blue: 75/255, alpha: 1)
        
        taxLabel.font = (UIFont(name: "Raleway-Medium", size: 17))
        taxLabel.textColor = UIColor(red: 75/255, green: 75/255, blue: 75/255, alpha: 1)
        displayTaxLabel.font = (UIFont(name: "Raleway-Medium", size: 20))
        displayTaxLabel.textColor = UIColor(red: 224/255, green: 20/255, blue: 79/255, alpha: 1)
        
        totalAmountLabel.font = (UIFont(name: "Raleway-Medium", size: 17))
        totalAmountLabel.textColor = UIColor(red: 75/255, green: 75/255, blue: 75/255, alpha: 1)
        
        homeLabel.font = (UIFont(name: "Raleway-Medium", size: 11))
        homeLabel.textColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)
        
        todayLabel.font = (UIFont(name: "Raleway-Medium", size: 11))
        todayLabel.textColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)
        
        calenderLabel.font = (UIFont(name: "Raleway-Medium", size: 11))
        calenderLabel.textColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)
        
        customerLabel.font = (UIFont(name: "Raleway-Medium", size: 11))
        customerLabel.textColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)

        moreLabel.font = (UIFont(name: "Raleway-Medium", size: 11))
        moreLabel.textColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
