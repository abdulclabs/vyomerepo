//
//  AppointmentDetailsViewController.swift
//  Vyometest
//
//  Created by Click Labs on 3/30/15.
//  Copyright (c) 2015 clabs. All rights reserved.
//

import UIKit

class AppointmentDetailsViewController: UIViewController {

    
    @IBOutlet weak var profileImage: UIImageView!
    
    @IBOutlet weak var appointmentTable: UITableView!
    
    @IBOutlet weak var moreUIView: UIView!
    
    @IBAction func cancelButton(sender: UIButton) {
        //show the alert message to win the match...
        let alert = UIAlertController(title: "CANCEL APPOINTMENT", message: "Do you want to cancel this appointment?", preferredStyle: UIAlertControllerStyle.Alert)
        
        alert.addAction(UIAlertAction(title: "YES", style: UIAlertActionStyle.Default, handler: { (alert:UIAlertAction!) -> Void in
        }))//end of alert add action...
        alert.addAction(UIAlertAction(title: "NO", style: UIAlertActionStyle.Default, handler: { (alert:UIAlertAction!) -> Void in
        }))
        self.presentViewController(alert, animated: true, completion: nil)
        //global variable...
    }
    
    @IBAction func markAsCompleteButton(sender: UIButton) {
        //show the alert message to win the match...
        let alert = UIAlertController(title: "CONFIRM", message: "Are you sure thet you want to mark this appointment as no show?\nNote: There's no undo!", preferredStyle: UIAlertControllerStyle.Alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (alert:UIAlertAction!) -> Void in
        }))//end of alert add action...
        alert.addAction(UIAlertAction(title: "CANCEL", style: UIAlertActionStyle.Default, handler: { (alert:UIAlertAction!) -> Void in
        }))
        self.presentViewController(alert, animated: true, completion: nil)
        //global variable...
        

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        moreUIView.backgroundColor =  UIColor(red: 174/255, green: 13/255, blue: 59/255, alpha: 1)
        self.appointmentTable.rowHeight = 50
        self.profileImage.layer.cornerRadius = self.profileImage.frame.size.width / 2;
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
